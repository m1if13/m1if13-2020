# TP 6 - Progressive Web Apps

## Objectifs

Créer une Progressive Web 
  - Permettre une "installation" de l'application. 
  - Effectuer des notifications
  - Structurer un App Shell et le mettre en cache
  - Utiliser des Services Workers pour mettre en cache du contenu de manière dynamique


## Pointeurs

  - [Les règles du jeu](https://forge.univ-lyon1.fr/LIONEL.MEDINI/m1if13-2020/#r%C3%A8gles-du-jeu)
  - Progressive Web Apps :
    - [MDN Progressive Web Apps](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps)
    - [Web.dev Progressive Web Apps](https://web.dev/progressive-web-apps/)
    - [Un générateur de manifeste](https://tomitm.github.io/appmanifest/)
  - Tester 
    - [LightHouse](https://developer.mozilla.org/en-US/docs/Tools/Responsive_Design_Mode)

## Application

Dans ce TP, vous continuerez de travailler sur l'application des TPs précédents. Cet énoncé part du principe que le client et l'API côté serveur fonctionnent :

- Votre client doit être packagé en webpack et déployé sur nginx.
- Votre client est une application Vue.js avec gestion des états et un routeur (côté client) qui permette de gérer les différentes vues de votre application.
- Une route côté serveur doit permettre d'accéder à l'API contenant le métier (actuel) de l'application.
- La partie mobile a été implémentée 

## Travail à réaliser


### Installation : Web App Manifest

Créez un fichier manifest qui permettent à votre application de "s'installer" sur mobile. 
Il faudra spécifier à minima un icone, un url, et des éléments descriptifs. 

Vous pouvez utiliser le panneau 'Application' des outils de développement Chrome pour inspecter le manifest.

Références :
  - [Spec W3C](https://www.w3.org/TR/appmanifest/)
  - [Doc MDN](https://developer.mozilla.org/en-US/docs/Web/Manifest)
  - [Doc Google](https://web.dev/add-manifest/)


### Notifications

Utiliser l'[API de notifications](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Re-engageable_Notifications_Push) pour notifier de la fin d'une partie.


En **option**: Vous pouvez l'API de push pour que le serveur signale à tous les participants via une notification les évènements majeurs de la partie (démarrage et fin).


### Mise en cache du AppShell

[Utiliser un Service Worker](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Offline_Service_workers) pour mettre en cache les fichiers du AppShell vu au 1e semestre: 
- Index et structure de l'application
- CSS
- images/logo

&#x26a0; **Remarque :** Pour pouvoir utiliser le service worker de l'app (en HTTPS sur votre VM) depuis votre PC ou téléphone, vous devez installer le [certificat racine](https://perso.liris.cnrs.fr/lionel.medini/enseignement/M1IF13/Certificats/MIF13-2019-2020-root-ca.crt).
- Sur PC, cela s'installe dans les paramètres du navigateur ; sur un téléphone, ça s'installe directement au niveau de l'OS.
- Pour Android : en s'envoyant le fichier par mail et en ouvrant la PJ, Android propose de l'installer directement.
- Pour IOS : suivre [ce lien](https://apple.stackexchange.com/questions/123988/how-to-add-a-crt-certificate-to-iphones-keychain) (merci Martin).


### Mise en cache avancée 

#### Optimisation du cache

Faites en sorte d'optimiser le chargement des _tiles_ par Leaflet. Par ordre de complexité :
1. Ajouter au cache celles qui correspondent à l'espace de jeu (cf. question précédente)
2. Intercepter les chargements dynamiques des _tiles_ et stocker les données (voir [cours](https://perso.liris.cnrs.fr/lmedini/wiki/doku.php?id=m1if13_2018_workers&do=export_revealjs#/10))
3. Précharger les _tiles_ voisines de celles actuellement affichées

**Aide** (pour les options 2 et 3) :

- &Agrave; vous de choisir (et d'indiquer dans votre readme) une stratégie de gestion du workflow entre service worker, cache et contenu de la page. Plusieurs stratégies sont possibles (et pas incompatibles) ; vous trouverez des explications et des références dans le [cours](https://perso.liris.cnrs.fr/lmedini/wiki/doku.php?id=m1if13_2018_workers#strategies_de_gestion_du_cache_1_4).
- Pour éviter d'entrer en conflit avec la gestion du cache réalisée à la question précédente, il est conseillé de placer les _tiles_ dans un objet JS plutôt que dans le cache. 

Remarque : dans le service worker, vous ne pourrez manipuler que des réponses "opaques", c'est-à-dire dont vous n'aurez accès ni au code de réponse, ni au statut. Mais cela ne vous empêchera pas de les cloner et de les renvoyer.

#### Gestion de la taille du cache

Au fur et à mesure que de nouvelles _tiles_ sont chargées, la taille des données stockées en cache / dans IndexedDB va augmenter. &Agrave; vous de proposer une solution pour limiter cette taille.
