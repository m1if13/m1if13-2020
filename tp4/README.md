# TP 4 - Frameworks et stockage de données côté client

## Objectifs

  - Prendre en main un framework en JS : Vue.js
  - Expérimenter les APIs de stockage de données en JS

## Pointeurs

  - Vue.js :
    - [page d'accueil](https://vuejs.org/)
    - [tutos](https://scrimba.com/playlist/pXKqta)
    - [doc](https://vuejs.org/v2/guide/)
    - [API](https://vuejs.org/v2/api/)
  - Vue-router :
    - [page d'accueil](https://router.vuejs.org/)
    - [exemples](https://github.com/vuejs/vue-router/tree/dev/examples)
  - [Vue-cli](https://cli.vuejs.org/)
  - VueX
    - [page d'acueil](https://vuex.vuejs.org/)
    - [exemple simple](https://vuex.vuejs.org/guide/)
    - [principes](https://vuex.vuejs.org/guide/state.html)
    - [installation](https://vuex.vuejs.org/installation.html)
 
## Application

Dans ce TP, vous continuerez de travailler sur l'application cartographique du TP3. Cet énoncé part du principe que le client et l'API côté serveur fonctionnent :

- votre client doit être packagé en webpack et déployé sur nginx : une requête sur la page d'accueil de votre proxy doit donc servir le fichier index.html, lequel doit "tirer" les scripts packagés, comme dans le répertoire `dist` (et plus depuis les CDNs)
- Une route doit permettre d'accéder à l'API contenant le métier (actuel) de l'application côté serveur

Dans ce TP, vous allez faire évoluer le client pour qu'il s'appuie sur un framework, et qu'il corresponde à l'API de jeu côté serveur.

## Outils

Pour faciliter le développement/debug, il est conseillé de :

- builder avec webpack en mode "développeur++" :

  ```javascript
  mode: 'development',
  devtool: 'source-map',
  watch: true,
  ```
- installer le plugin [Vue.js devtools](https://github.com/vuejs/vue-devtools) dans votre navigateur

## Travail à réaliser

### Mise en place de Vue.js

Dans cette partie, vous allez progressivement mettre en place le framework Vue.js dans votre client, sans en changer le métier.

- Rajoutez Vue.js comme dépendance de votre projet et faites en sorte que le framework se charge au démarrage de l'application.

  **Aide** : pour [utiliser Vue avec des modules ES6](https://vuejsdevelopers.com/2019/02/04/vue-es-module-browser-build/) (en mode dévelopment), vous avez intérêt à importer le fichier `node_modules/vue/dist/vue.esm.browser.js`. 

- Créez des composants pour les parties principales de votre application (démarrage de l'application, formulaire d'envoi des données, carte), ainsi que pour les sous-éléments de ces composants liés à un modèle (notifications, indicateur de zoom). Mettez en place les modèles, les templates et les composants pour faire fonctionner l'application comme précédemment.

  **Aide** :
  - pour pouvoir inclure des composants à un niveau quelconque de l'arborescence DOM, il faut les déclarer explicitement dans l'application Vue, à l'aide de la propriété `components` : https://stackoverflow.com/questions/39382032/vue-js-unknown-custom-element
  - pour communiquer programmatiquement en 2-way binding entre un composant et la racine de l'application, ou entre plusieurs composants applicatifs, il faut utiliser les [Custom Events](https://vuejs.org/v2/guide/components-custom-events.html) de vue. Un bon exemple dans [ce tutoriel](https://flaviocopes.com/vue-components-communication/#using-an-event-bus-to-communicate-between-any-component). Ainsi, vous pouvez donc définir :
    - des événements "globaux" (event bus) accessibles par toute l'application,
    - des événements "locaux" liés à un sous-arbre de composants, en les "bindant" au composant de plus haut niveau souhaité : `this.$on()` dans ce composant.
    - quand vous rajoutez dynamiquement des éléments HTML à des composants dans des fichiers .vue, ils ne peuvent pas utiliser les styles définis pour votre composant, car ces styles sont "scoped" -> il faut rajouter l'attribut `data` du composant avec `vm.$options._scopeId` (voir [ici](https://github.com/vuejs/vue-loader/issues/559)).

### State Management Pattern

Nativement, Vue.js fait du one-way data binding "progressif". La vue se met automatiquement à jour à chaque changement du modèle. Les actions de l'utilisateur qui impliquent une modification du modèle doivent être explicitement déclarées et implémentées. **Vuex** implémente le pattern de gestion d'états ; il vous permet d'abstraire le comportement et les concepts qui régissent ce type de fonctionnement. Concrètement, il vous sera beaucoup plus facile de gérer vos données de manière centralisée que de "cascader" les dépendances dans la hiérarchie de composants.

Dans cette partie, vous allez utiliser VueX pour gérer "proprement" les états et les transitions de votre application. Si vous devez utiliser un store, il est conseillé de le faire le plus tôt possible, afin de ne pas perdre trop de temps à résoudre des problèmes de data binding entre composants que vous n'aurez plus à vous poser par la suite.

  - Comme précédemment, ajoutez `vuex` aux dépendances de votre application, déclarez un `store` à la racine de votre application, et faites en sorte que tous les composants en héritent.
  - Définissez un `state` (arbre d'éléments du modèle) que vous allez stocker dans le store vuex, et déclarez-y les variables de votre application (inutile d'y mettre les données immutables). 
  - Dans vos composants, [récupérez les données du modèle depuis le store](https://vuex.vuejs.org/guide/state.html#getting-vuex-state-into-vue-components) pour les utliser dans vos templates et vos méthodes.
  - Identifiez les `mutations` (transitions) et les `actions` (qui vont permettre de `commit` ces mutations), et remplacez les `props` de vos composants par des [accès aux données du store](https://vuex.vuejs.org/guide/state.html#getting-vuex-state-into-vue-components) ou des commits dans des [propriétés `computed`](https://vuejs.org/v2/api/#computed).
  - Appelez ces mutations et/ou ces actions depuis les éléments appropriés de vos composants. Il est conseillé de les appeler dans les callbacks des événements que vous aurez définis.

### Routeur

Dans cette partie, vous allez transformer votre application en Single-Page Application en utilisant le routeur de Vue.

**Remarque** : le routeur de Vue permet de faire du routage côté client, même si les paths commencent par un slash (les URLs sont en http://.../#/route).

  - Ajoutez `vue-router` aux dépendances de votre application.
  - Créez la configuration de votre routeur pour qu'elle permette d'afficher les composants qui doivent s'afficher alternativement dans la vue : lancement d'une partie, carte et formulaire contenant les coordonnées en texte.
  - Ajoutez un menu dans votre application pour pouvoir les faire réapparaître.

**Aide** : contrairement à l'exemple simple qui vous a été présenté en M1IF03, le routeur de Vue ne se contente pas de changer les propriétés CSS des éléments qui ne sont pas dans la route courante pour ne pas qu'ils soient visibles. Il les supprime du DOM et les met en les "cache". En conséquence, certaines parties de vos composants ne seront pas accessibles quand les composants sont "arrêtés" et les hooks liés à leur cycle de vie sont rappelés quand vous revenez sur la route à laquelle ils appartiennent. De la même façon, un composant n'a pas été initialisé tant que sa route n'a pas été activée. Cela pose plusieurs problèmes :

  - Leaflet n'est pas prévu à l'origine pour être affiché dans un composant Vue, et __a fortiori__ pas dans une SPA avec un routeur qui "éteint" les composants quand ils ne sont pas affichés. Il faudra prévoir de créer puis de replacer la map aux bonnes coordonnées et au bon niveau de zoom chaque fois que la route change et qu'elle doit être réaffichée.
  - Si certains éléments comme les cercles sont automatiquement réaffichés avec la map (vous pouvez donc les ajouter avec `mapTo()` et ne plus vous en préoccuper), les markers ne fonctionnent pas de la même façon (non documenté). Pour afficher des markers "routing-proof", vous devrez donc :
    - mémoriser (localement ou dans le store) les données de chacun des markers
  - recréer ces markers et les ajouter à la map à chaque nouvel affichage (hook : `bounded`) du composant
  - supprimer ces markers et les retirer de la map à chaque changement de route et arrêt (hook : `beforeDestroy`) du composant
  - Apparemment, les bindings des événements dans Vue ne sont pas gérés de la même façon en fonction de leur target :
    - les bindings aux événements globaux (Event bus) sont conservés entre les créations du composant, même si ce composant est démarré et arrêté par le routeur. Donc si vous faites un binding dans un hook (`created` ou `mounted`), vous déclencherez autant de fois le callback que vous aurez redémarré le composant. Solution : mettre dans le module un booléen qui vérifie que les bindings n'ont été faits qu'une fois.
  - les bindings des événements locaux au composant sont perdus quand on change de route. Il faut donc bien les définir dans le hook `mounted`.

### Amélioration de l'application

Vous allez maintenant modifier votre client pour qu'il corresponde au jeu en "multi-joueurs". L'idée est que chaque joueur puisse :
- se connecter (__via__ Spring)
- envoyer au serveur Node l'URL d'une image le représentant
- définir ses coordonnées sur la carte et les envoyer au serveur
- requêter la liste des informations sur les joueurs (logins, positions, images, rôles, statuts) et sur les autres éléments du jeu et les afficher sur la carte

- afficher la liste des trophées de l'utilisateur local

Indications :

- les positions des éléments "blurred" seront indiquées par un cercle qui matérialise la zone où se trouve possiblement l'élément
- pendant le déroulement du jeu, un composant Vue permettra au joueur local de visualier son rôle, avec éventuellement le TTL qui lui reste
- une vue spéciale (modale) permettra d'afficher le fait que l'utilisateur local vient de gagner ou perdre la partie


Pour cela, vous synchroniserez le store Vuex avec les supports de persistance côté client, en utilisant une approche à base de listeners liés aux `mutations` ou aux `actions` Vuex. De la même manière, implémentez les actions nécessaires pour recharger le store en fonction des données locales au lancement de l'application.

**Aide** : `localStorage` et `sessionStorage` ne permettent de stocker que des chaînes de caractères. Pour mémoriser des objets complexes, vous devrez utiliser `JSON.parse()` et `JSON.stringify()`.