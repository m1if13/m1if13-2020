# TP 3 - prise en main de la stack JS

## Objectifs

Comprendre le rôle et utiliser les principaux outils de gestion de projet en JS

  - NPM : gestion de dépendances côté serveur
  - Webpack : packaging du code, exécution de tâches
  - Node : exécution côté serveur
  - Express : serveur Web

## Pointeurs

Documentation et tutos :

  - NPM : [créer un projet](https://docs.npmjs.com/creating-a-package-json-file)
  - [Express](https://expressjs.com/) :
    - [lancer un serveur](https://expressjs.com/en/starter/hello-world.html)
	- [servir des fichiers statiques](https://expressjs.com/en/starter/static-files.html)
	- [Routage (répondre à des URLs spécifiques)](https://expressjs.com/en/guide/routing.html)
	- [Traiter des données de formulaire](https://www.npmjs.com/package/body-parser)
 - Webpack :
    - [installation](https://webpack.js.org/guides/installation/)
	- [packager un projet](https://webpack.js.org/guides/getting-started/)
	- [créer et gérer des `assets`](https://webpack.js.org/guides/asset-management/)
  - Leaflet (OpenStreetMap) : [utiliser la carte](https://leafletjs.com/examples/quick-start/)
 
## Application

Dans ce TP et les suivants, vous allez travailler sur une application cartographique simple, que vous transformerez progressivement en une application de jeu en Web mobile et multi-joueurs.

Une version de base pour le client de cette application vous est fournie dans ce dépôt et dans le répertoire `tp3/src` (le fichier zip contient tous les autres fichiers). Copiez ces fichiers sur votre machine (sans mettre les fichiers dans votre dépôt pour l'instant), ouvrez le fichier index.html dans votre navigateur et vérifiez que tout fonctionne bien : toute modification ou validation du formulaire doit provoquer un déplacement / changement de zoom sur la carte. Examinez le code pour le comprendre et pour connaître les dépendances.

&Agrave; la fin de ce TP, deux joueurs devront pouvoir s'identifier, échanger leurs positions via un serveur Node/Express et visualiser ces positions sur la carte.

## Travail à réaliser

### Initialisation

  - Créez un dossier `app` à la racine de votre dépôt de code. &Agrave; partir de ce TP, vous travaillerez dans ce dossier.
    - Créez un sous-dossier `app/client` dans lequel vous placerez ce qui concerne la SPA (sources du TP3)
    - Créez un sous-dossier `app/server` dans lequel vous placerez ce qui concerne le serveur Express
  - Dans chacun de ces sous-dossiers, créez un projet NPM à l'aide du tuto mentionné plus haut.<br>
    Attention : utilisez la commande `npm init` sans l'option `--yes` pour pouvoir entrer autre chose que les valeurs par défaut.
  - &Agrave; l'aide de [ce template](https://github.com/github/gitignore/blob/master/Node.gitignore), complétez le fichier `.gitignore` de votre dépôt avec les noms de fichiers et répertoires à ignorer pour votre projet (_a minima_ le répertoire `node_modules` et les fichiers de votre IDE).
  - **Avant de pusher sur la forge, vérifiez bien que le répertoire node_module n'est pas inclus dans votre `git status`.**

## Partie client

Cette partie sera développée dans les TPs suivants. Pour l'instant, créez un sous-répertoire où vous copierez les sources du projet.

### Packaging

  - Intégrez les dépendances de la page principale au projet en les cherchant dans NPM et en les installant **localement**. Remplacez les requêtes aux CDNs (ce n'est pas une bonne pratique, mais c'est pour l'exercice) par des requêtes dans le repository local (`node_modules`).
  - Ajoutez Webpack aux dépendances de développement du projet, comme indiqué dans le tutoriel d'installation.
  - Générez le bundle correspondant à votre projet et créez un fichier de configuration Webpack pour le définir en tant que module.

Aide : pour donner accès facilement à jQuery (qui est utilisé dans `form.js` et `map.js`), vous pouvez utiliser `ProvidePlugin` : https://stackoverflow.com/questions/28969861/managing-jquery-plugin-dependency-in-webpack

  - rajoutez une commande `npm run build` dans le fichier package.json pour pouvoir lancer le build facilement. Vous pouvez rajouter d'autres commandes qui simplifient le déploiement (watch...), mais dans ce cas, indiquez dans votre readme quelle commande doit être exécutée pour lancer votre applcation.
  - Utilisez le tutoriel sur les assets de Webpack pour produire plusieurs modules (en incluant aussi les CSS) qui dépendront les uns des autres.

Aide :
  - Pour que jQuery soit disponible nativement dans toute l'application, vous pouvez utiliser (`webpack.ProvidePlugin`)[https://webpack.js.org/plugins/provide-plugin/]
  - Il y a une issue non résolue dans Leaflet  : https://github.com/Leaflet/Leaflet/issues/6496 qui provoque le problème suivant : une fois les icônes fournis avec Leaflet packagés avec webpack, ils ne sont pas retrouvés par la lib à cause d’une erreur de regexp. Le workaround est de rajouter dans map.js (uniquement dans la version webpack) une ligne comme :
  ```javascript
  L.Icon.Default.imagePath` = '/lib/leaflet/dist/images/';
  ```

où la partie droite permet d'accéder par une requête HTTP au répertoire contenant les icônes Leaflet.

&Agrave; la fin de cette partie, webpack a dû vous générer un répertoire dist contenant des fichiers statiques que vous pouvez exécuter dans votre navigateur. Ce sont ces fichiers que vous déploierez sur votre VM.

## Partie serveur

Dans `app/server`, créez un répertoire `public` et placez-y le client fourni dans les sources de ce TP.

### Serveur de fichiers statiques

Vous allez utiliser Express pour le back-office de votre application.

- Ajoutez à votre projet une dépendance vers la dernière version d'Express
- Utilisez les tutos sur Node / Express pour démarrer un serveur Web (en mode Hello World).
- Faites en sorte que votre serveur Express réponde aux requêtes sur `/static` en servant directement les fichiers situés dans le répertoire `public`.
- Ajoutez un [middleware de gestion des erreurs 404](http://expressjs.com/en/starter/faq.html#how-do-i-handle-404-responses)
- Testez.

### Contenus dynamiques

Vous allez mettre en place 2 parties de votre application capables de [servir des contenus dynamiques](http://expressjs.com/en/starter/basic-routing.html), une pour l'interface d'administration, et une autre pour gérer le fonctionnement du jeu.

Pour chacune de ces 2 parties de l'application, vous créerez un middleware spécifique, à l'aide de la classe [`express.Router`](http://expressjs.com/en/guide/routing.html#express-router), et l'associerez à une route spécifique (voir partie Déploiement). Vous placerez le code de ces routes dans un répertoire `routes`.

#### Gestion du fonctionnement du jeu

Cette partie de l'application sera requêtée par le client. Vous ne mettrez donc en place qu'une API côté serveur. Cette API va gérer les ressources géolocalisées : les utilisateurs et d'autres éléments placés aléatoirement par le serveur.

Les accès aux ressources se feront ainsi :
- mise à jour de l'URL de l'image associée à l'utilisateur (et qui sera géolocalisée sur la carte)
- mise à jour de la position de l'utilisateur
- récupération de la liste complète des objets à afficher

L'API du serveur est donnée dans le fichier [`express-api.yaml`](./express-api.yaml). Vous pouvez vous aider de la génération de serveurs pour générer le squelette de votre code.

&Agrave; la réception de chaque requête, n'oubliez pas de valider l'identité de l'utilisateur avec le serveur Spring (aide : voir interface d'administration).

#### Interface d'administration

Il s'agit ici de mettre en place des pages générées côté serveur pour :
- créer, visualiser, modifier, supprimer des utilisateurs : mettez en place les pages et les formulaires, ainsi que l'interrogation du serveur Spring pour réaliser ces opérations
- paramétrer et suivre le déroulement d'une partie : recopiez et réutilisez les sources de l'application fournie et faites en sorte qu'on puisse visualiser les positions de tous les joueurs (voir TPs suivants)

Au moment du paramétrage d'une partie, cette interface d'administration devra afficher :
  - la carte (avec la position des joueurs) pour fixer :
    - le périmètre de jeu (par exemple, en spécifiant 2 points, générer un cercle dont ces points sont le diamètre)
    - la position de la cible (spécifier un point et a)
  - un formulaire pour :
    - fixer le `ttl` (valeur par défaut : 1 minute)
    - Démarrer la partie

Une fois la partie démarrée, cette interface permettra de suivre la position des joueurs et d'indiquer quand et par qui elle a été remportée.

**Indications** :
- pour le templating des pages côté serveur, vous pouvez utiliser [EJS](https://github.com/tj/ejs) ; vous placerez tous les templates dans un répertoire spécifique
- pour le requêtage du serveur Spring, vous pouvez utiliser [Axios](https://www.npmjs.com/package/axios)
- pour éviter de vous e...er avec la configuration d'Axios lorsque vous requêtez le serveur Spring (dont le certificat est signé par une autorité qu'il ne connaît pas), vous pouvez le requêter sur le port 8080 plutôt qu'en passant par nginx.

## Déploiement

Déployez la partie client (répertoire `dist`) dans sur le serveur front (nginx) de votre VM.

Configurez votre serveur Express pour qu'il réponde sur le port 3376.
Déployez la partie serveur de votre projet sur votre VM et faites deux routes sur votre proxy nginx pour pouvoir l'interroger :

- sur l'adresse de votre VM + `/admin` pour l'interface d'administration
- sur l'adresse de votre VM + `/public` pour les contenus statiques (pour faciliter les tests)
- sur l'adresse de votre VM + `/api` pour les contenus dynamiques

Normalement, vous devriez pouvoir aussi interroger votre serveur Express sur le port 3376.

Comme au premier semestre, déployez une version HTTPS de votre serveur nginx (mais conservez la version HTTP). Vous trouverez les certificats [ici](https://perso.liris.cnrs.fr/lionel.medini/enseignement/M1IF13/Certificats/).