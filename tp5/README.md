# TP 5 - Web Mobile

## Objectifs

  - Adapter votre application web pour des vues mobiles
  - Accéder aux capteurs du téléphone
  - Implémenter les [règles du jeu](https://forge.univ-lyon1.fr/LIONEL.MEDINI/m1if13-2020/#r%C3%A8gles-du-jeu)

Nous verrons les Progressive Web Apps lors du prochain TP. Pas la peine de s'attaquer à cela pour le moment.

## Pointeurs

  - [Les règles du jeu](https://forge.univ-lyon1.fr/LIONEL.MEDINI/m1if13-2020/#r%C3%A8gles-du-jeu)
  - Vue et mobile :
    - [Vuetify](https://vuetifyjs.com/en/) une bibliothèque de composants appliquant les principes du Material Design.
  - Capteurs
    - [Simuler sa localisation](https://developers.google.com/web/tools/chrome-devtools/device-mode/geolocation)
    - [LocationGuard](https://github.com/chatziko/location-guard)
    - [Generic Sensor Demos](https://intel.github.io/generic-sensor-demos/) à tester avec différents dispositifs et navigateurs.
  - Tester 
    - [Simulation mobile des Chrome Dev Tools](https://developers.google.com/web/tools/chrome-devtools/device-mode)
    - [Remote debugging toujours avec les Chrome Dev Tools](https://developers.google.com/web/tools/chrome-devtools/remote-debugging)
    - [Responsive Design Mode de Firefox](https://developer.mozilla.org/en-US/docs/Tools/Responsive_Design_Mode)

## Application

Dans ce TP, vous continuerez de travailler sur l'application des TPs précédents. Cet énoncé part du principe que le client et l'API côté serveur fonctionnent :

- Votre client doit être packagé en webpack et déployé sur nginx.
- Votre client est une application Vue.js avec gestion des états et un routeur (côté client) qui permette de gérer les différentes vues de votre application.
- Une route côté serveur doit permettre d'accéder à l'API contenant le métier (actuel) de l'application.


## Outils

Pour faciliter le développement/debug, il est conseillé de :


## Travail à réaliser

Commencez par (re)lire les [règles du jeu](https://forge.univ-lyon1.fr/LIONEL.MEDINI/m1if13-2020/#r%C3%A8gles-du-jeu) pour être au clair sur la logique de l'application.

### Ajout d'une bibliothèque de composants

Revoir vos composants pour qu'ils s'appuient sur une bibliothèque standard de composants fonctionnant sur mobile.

Nous vous encourageaons à utiliser [Vuetify](https://vuetifyjs.com/en/) qui s'appuie sur les principes de Material Design.


### App Shell
Comme nous l'avons vu en M1IF03, un app shell permet de créer une coquille de l'application qui sera chargée rapidement et mis en cache. 

Nous allons créer un [App Shell](https://developers.google.com/web/fundamentals/architecture/app-shell) qui nous servira lors du TP pwa à venir, en s'appuyant sur les composants de la bibliothèque que vous utilisez. Nous verrons sa mise en cache plus tard.

### Géolocalisation inverse

Un administrateur (ou une IA) doit pouvoir définir une cible à atteindre sur la carte. Cette cible sur la carte doit être convertie en position (lat, long). Voir la [doc de leaflet](https://leafletjs.com/reference-1.6.0.html#mouseevent-latlng)


### Capteurs et géolocalisation

Afficher la position des différents joueurs sur la carte. Cela peut être simulé par différentes pages ouvertes en même temps. Voir les outils de [simulation de localisation](https://developers.google.com/web/tools/chrome-devtools/device-mode/geolocation) listés plus haut.

Utilisez [`watchPosition`](https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/watchPosition) pour notifier votre application de changement de position.

Regarder la [documentation de vuex](https://vuex.vuejs.org/guide/actions.html#dispatching-actions) pour notifier le serveur d'un changement de position 


### Synchroniser client et serveur

Au niveau des actions de vuex, faire un polling à intervalle régulier du serveur pour mettre à jour les positions des autres joueurs.

### Vue victoire/défaite

Une vue spéciale (modale) permettra d'afficher le fait que l'utilisateur local vient de gagner ou perdre la partie

Faire [vibrer le téléphone](https://developer.mozilla.org/en-US/docs/Web/API/Vibration_API) en cas de fin de partie. 


<!-- ### Vue score
La vue score doit permettre d'afficher les points des différents joueurs, et avoir une route côté client qui lui soit associée. Utiliser des composants de liste qui s'affiche proprement sur mobile, et sur ordinateur. -->


### Vie

Pendant le déroulement du jeu, un composant Vue permettra au joueur local de visualiser son rôle, avec éventuellement le TTL qui lui reste.


### En bonus

#### Thème et luminosité
Nous allons changer le thème de l'appliation en fonction de la [luminosité ambiante](https://www.w3.org/TR/ambient-light/) pour pouvoir jouer dans le noir.

Pour cela il faut [autoriser l'accès aux generic sensors]([chrome://flags/#enable-generic-sensor-extra-classes](chrome://flags/#enable-generic-sensor-extra-classes)

Des [explications pour vuetify]( https://www.reddit.com/r/vuetifyjs/comments/cixlfr/vuetify_20_darklight_background_swap/), mais cela doit fonctionner avec d'autres frameworks.

#### Visibilité de la carte

Les positions des éléments "blurred" seront indiquées par un cercle qui matérialise la zone où se trouve possiblement l'élément


### Direction des joueurs
Quand les joueurs bougent indiquer leur direction. Plusieurs approches sont possibles: 
	- calculer la direction en se basant sur la position courante et la position passée 
	- utiliser la boussole (le magnétomètre) du téléphone. Le magnétomètre est protégé pour des raisons de protection de la vie privée. Il faut l'activer dans les options de Chrome. [chrome://flags/#enable-generic-sensor-extra-classes](chrome://flags/#enable-generic-sensor-extra-classes)
	- d'autres approches s'appuyant sur les valeurs de l'accéléromètre sont possibles.



