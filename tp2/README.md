# TP Spring Boot plugins

## Objectifs pédagogiques

- Mettre en place une page d'erreur templatée
- Générer automatiquement une documentation d'API à l'aide d'annotations OpenAPI
- Comprendre et configurer les mécanismes CORS
- Utiliser l'API Web Storage côté client

Dans ce TP, vous allez continuer la mise en place du serveur de gestion d'utilisateurs et d'authentification commencé au serveur précédent. Le métier de ce serveur ne va pas changer, mais vous allez l'enrichir en prenant en compte des préoccupations non métier, mais néanmoins incontournables : gestion des erreurs, documentation, requêtage cross-domain.

Vous créerez ensuite un client très simple capable de loguer un utilisateur et de mémoriser ses credentials.

## Gestion des erreurs

Mettez en place, en utilisant Thymeleaf, une page `error.html`, qui indiquera le code et le message des erreurs rencontrées. Testez.

## Génération de la documentation

&Agrave; l'aide d'annotations OpenAPI, générez une documentation de l'API du serveur.

Ressources :
- [Spécifications / Getting started](https://github.com/springdoc/springdoc-openapi)
- [Exemple de tutoriel assez complet](https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations)
- [Javadoc](http://docs.swagger.io/swagger-core/v2.1.1/apidocs/)

**Indications** :
- Attention, il faut indiquer la dernière version de springdoc-openapi dans votre pom.xml : **1.2.32**
- Pour "supprimer" le type de contenu de la documentation d'une erreur HTTP, il faut rajouter explicitement une annotation `@Content()` vide
- Si vous utilisez plusieurs fois la même annotation, seule la dernière sera prise en compte. En conséquence, pour pouvoir documenter deux méthodes répondant à la même URL (et par exemple générant différents types de contenus), il faut mettre toute la documentation dans une seule annotation.
- Voir [cette FAQ](https://springdoc.github.io/springdoc-openapi-demos/faq.html#how-can-i-define-multiple-openapi-definitions-in-one-spring-boot-project) pour grouper plusieurs contrôleurs dans la même documentation

## Mise en place de CORS avec Spring Boot

**Attention : l'exemple de code OperationController du TP1 a été modifié. Le logout s'effectuera avec une méthode DELETE et non POST comme initialement indiqué. Merci de mettre à jour votre contrôleur en conséquence.**

Utilisez les annotations fournies par le [CORS support](https://jira.spring.io/browse/SPR-9278?redirect=false) de Spring pour supporter CORS de la façon suivante :
- Autoriser les origines `http://localhost`, `http://192.168.75.XX`, et `https://192.168.75.XX`, où XX est la fin de l'IP de votre VM, pour les requêtes :
  - `POST /login`
  - `GET /authenticate`
  - `DELETE /logout`
  - `GET /user/{login}`

**Aide :** Pour autoriser les scripts d'un client CORS à récupérer le header "Authentication" qui contient le token JWT, vous devrez ajouter un header [Access-Control-Expose-Headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Expose-Headers) aux réponses avec succès aux POST sur '/login'.

Ressources :
- [Tutoriel](https://spring.io/guides/gs/rest-service-cors/) (trop) complet
- [Billet de blog](https://spring.io/blog/2015/06/08/cors-support-in-spring-framework) limité aux annotations, mais pas à jour

Testez, par exemple à partir d'un serveur que vous installerez sur le port 80 de votre machine.

## Conception et déploiement d'un client

Créez un client simple (une page HTML + un script) capable de :
- connecter un utilisateur : envoi des login/password et récupération d'un token
- stoker localement :
  - le token JWT en `SessionStorage`
  - le login de l'utilisateur en `LocalStorage`
- permettre à l'utilisateur d'effacer ces données
- vérifier la connexion de cet utilisateur
- déconnecter l'utilisateur

Ci-dessous, un exemple d'interface :

![Interface client](./client.png)

Déployez ce client sur le front nginx (et non sur Spring). Testez-en le fonctionnement avec CORS.

## Instructions de rendu

Les TPs 1 et 2 sont à rendre pour le **lundi 9 mars 2020 à 23h59**.

Votre dépôt de code doit comporter les éléments suivants :
- un répertoire `users` à la racine, contenant le code de la partie Spring Boot
- un fichier `users-api.yaml`, également à la racine du dépôt, contenant la documentation OpenApi générée
- un répertoire `simple-client`, toujours à la racine, contenant le client
- un fichier readme avec une section "TP1 & TP2", contenant les liens vers :
  - le fichier YAML sur le dépôt
  - le Swagger généré par Spring et déployé sur votre VM (et qui doit permettre de tester votre serveur)
  - le client simple déployé sur le serveur nginx de votre VM (et qui doit requêter votre serveur)