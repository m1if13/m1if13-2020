# M1IF13-2020

Ce dépôt contient les énoncés et le code des TP de Web avancé et mobile pour 2019-2020.

## Description de l'application

**Attention : nouvelles règles**

L'application décrite ci-dessous est en "mode dégradé" par rapport à ce qui a été prévu initialement.

### Règles du jeu

Le jeu se joue à un ou plusieurs joueurs, dans un périmètre géolocalisé et délimité par un administrateur du jeu.
Il consiste pour les joueurs à atteindre en un temps maximum un point de l'espace de jeu, dénoté par une "cible" positionnée par l'administrateur.
Si plusieurs joueurs sont en compétition, celui qui atteint la cible en premier gagne la partie et les autres sont éliminés. On peut imaginer un jeu en plusieurs manches, où les vainqueurs des manches précédentes s'affrontent entre eux.
Si une partie se joue avec un seul joueur, à chaque manche réussie, il peut continuer à jouer et doit atteindre une nouvelle cible en un temps inférieur.

Dans ce qui suit, on considère que "atteindre une cible" consiste pour un utilisateur à se déplacer à moins de 2 mètres d'un élément du jeu.

#### Le jeu gère des `GeoResource` de deux types :

- les utilisateurs : ils doivent être préalablement inscrits sur le serveur de gestion des utilisateurs. Dès qu'ils sont connectés, leurs positions apparaissent sur les différentes interfaces (d'administration et de jeu) de l'application. Les positions des joueurs adverses sont floutées (blurred).
- les cibles : une cible est positionnée par l'administrateur avant chaque chaque manche.

#### Les administrateurs peuvent paramétrer la difficulté du jeu :

- en ajustant le `ttl` ; valeur par défaut : 1 minute. Ce `ttl` diminue à chaque manche.

Avant de modifier l'interface d'administration, vous pouvez utiliser les valeurs par défaut, que vous stockerez dans des constantes côté serveur.

### Architecture

Cette application s'appuiera sur plusieurs serveurs pour réaliser différents types de traitements :

- un serveur de gestion des utilisateurs et des tokens de connexion (Java / Tomcat / Spring Boot),
- un serveur de fichiers statiques côté client qui fera aussi office de reverse proxy (nginx),
- un serveur applicatif qui permettra de réaliser le métier de l'application (JS / Node / Express).

Côté client, il y aura plusieurs applications distinctes :

- un client HTML simple qui permet de créer et de gérer les utilisateurs
- une interface d'administration du jeu générée dynamiquement par le serveur applicatif (HTML / CSS)
- un client sous forme de SPA qui sera utilisé par les joueurs ; ce client sera un mashup des différents serveurs (JS / Webpack / Vue)

La structure globale de l'application est illustrée ci-dessous

![Schéma de l'architecture de l'application](Archi.png)

## TPs

- [TP1](tp1) : serveur de gestion des utilisateurs en Spring Boot + infra
- [TP2](tp2) : client Spring Boot en CORS
- [TP3](tp3) : serveur métier en Node / Express + interface admin
- [TP4](tp4) : client de jeu en Vue
- [TP5](tp5) : Web mobile + device API
- [TP6](tp6) : Progressive Web App
- [TP7](http://champin.net/2019/wasm/) : Web Assembly
